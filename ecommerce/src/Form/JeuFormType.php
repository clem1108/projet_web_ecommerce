<?php

namespace App\Form;

use App\Entity\Jeu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JeuFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                "attr" => [
                    "class" => "input_nom",
                    "placeholder" => "Nom"
                ]
            ])
            ->add('description', TextareaType::class, [
                "attr" => [
                    "class" => "input_description",
                    "placeholder" => "Description"
                ]
            ])
            ->add('prix', NumberType::class, [
                "attr" => [
                    "class" => "input_prix",
                    "placeholder" => "Prix"
                ]
            ])
            ->add('quantite', IntegerType::class, [
                "attr" => [
                    "class" => "input_quantite",
                    "placeholder" => "Quantité"
                ]
            ])
            ->add("Envoyer", SubmitType::class, [
                "attr" => [
                    "role" => "button",
                    "class" => "btn bouton",
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Jeu::class,
        ]);
    }
}
