<?php

namespace App\Form;

use App\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UtilisateurFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                "attr" => [
                    "class" => "input_email",
                    "placeholder" => "Email"
                ]
            ])
            ->add('prenom', TextType::class, [
                "attr" => [
                    "class" => "input_prenom",
                    "placeholder" => "Prénom"
                ]
            ])
            ->add('nom', TextType::class, [
                "attr" => [
                    "class" => "input_nom",
                    "placeholder" => "Nom de Famille"
                ]
            ])
            ->add('date_naissance', BirthdayType::class, [
                "attr" => [
                    "class" => "input_date_naissance",
                    "placeholder" => "Date de Naissance",
                ],
                "format" => "ddMMyyyy",
            ])
            ->add("Modifier", SubmitType::class, [
                "attr" => [
                    "role" => "button",
                    "class" => "btn bouton",
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}
