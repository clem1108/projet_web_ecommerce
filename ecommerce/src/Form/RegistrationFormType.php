<?php

namespace App\Form;

use App\Entity\Utilisateur;
use DateTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                "attr" => [
                    "class" => "input_email",
                    "placeholder" => "Email"
                ]
            ])
            ->add('prenom', TextType::class, [
                "attr" => [
                    "class" => "input_prenom",
                    "placeholder" => "Prénom"
                ]
            ])
            ->add('nom', TextType::class, [
                "attr" => [
                    "class" => "input_nom",
                    "placeholder" => "Nom de Famille"
                ]
            ])
            ->add('date_naissance', BirthdayType::class, [
                "attr" => [
                    "class" => "input_date_naissance",
                    "placeholder" => "Date de Naissance",
                ],
                "format" => "ddMMyyyy",
                "data" => new DateTime("2001-01-01"),
            ])
            ->add('plainPassword', PasswordType::class, [
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 4,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        'max' => 4096,
                    ]),
                ],
                "attr" => [
                    "class" => "input_mot_passe",
                    "placeholder" => "Mot de Passe"
                ]
            ])
            ->add("Inscription", SubmitType::class, [
                "attr" => [
                    "role" => "button",
                    "class" => "btn bouton",
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}
