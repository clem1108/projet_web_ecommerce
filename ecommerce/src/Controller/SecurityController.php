<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Form\MotPasseFormType;
use App\Form\RegistrationFormType;
use App\Form\SoldeFormType;
use App\Form\UtilisateurFormType;
use App\Security\ConnexionAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $outils): Response
    {
        $erreur = $outils->getLastAuthenticationError();
        $nom_utilisateur = $outils->getLastUsername();

        return $this->render('security/login.html.twig', ['nom_utilisateur' => $nom_utilisateur, 'erreur' => $erreur]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('Cette méthode peut être vide - elle sera interceptée par la clé de déconnexion sur votre pare-feu.');
    }



    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $requete, UserPasswordEncoderInterface $encode, GuardAuthenticatorHandler $garde, ConnexionAuthenticator $indentification, EntityManagerInterface $manager_interface): Response
    {
        $utilisateur = new Utilisateur();
        $formulaire = $this->createForm(RegistrationFormType::class, $utilisateur);
        $formulaire->handleRequest($requete);

        if ($formulaire->isSubmitted() && $formulaire->isValid()) {
            $utilisateur->setPassword(
                $encode->encodePassword(
                    $utilisateur,
                    $formulaire->get('plainPassword')->getData()
                )
            );
            $utilisateur->setSolde(0);
            $manager_interface->persist($utilisateur);
            $manager_interface->flush();

            return $garde->authenticateUserAndHandleSuccess(
                $utilisateur,
                $requete,
                $indentification,
                'main'
            );
            return $this->redirect('/');
        }

        return $this->render('security/register.html.twig', [
            'formulaire' => $formulaire->createView(),
        ]);
    }

    /**
     * @Route("/modification/{id}", name="utilisateur.modification")
     */
    public function modification_utilisateur(Utilisateur $utilisateur, Request $requete, GuardAuthenticatorHandler $garde, ConnexionAuthenticator $indentification, EntityManagerInterface $manager_interface): Response
    {

        $formulaire = $this->createForm(UtilisateurFormType::class, $utilisateur, [
            "attr" => [
                "class" => "formulaire_modification_utilisateur",
            ]
        ]);

        $formulaire->handleRequest($requete);
        if ($formulaire->isSubmitted() && $formulaire->isValid()) {
            $manager_interface->persist($utilisateur);
            $manager_interface->flush();

            if ($utilisateur->getRoles() == ["ROLE_USER"]) {

                return $garde->authenticateUserAndHandleSuccess(
                    $utilisateur,
                    $requete,
                    $indentification,
                    'main'
                );
                return $this->redirect('/profil');
            } else {
                return $this->redirect('/gestion');
            }
        }

        return $this->render('security/modification.html.twig', [
            'formulaire' => $formulaire->createView(),
            'utilisateur' => $utilisateur,
        ]);
    }

    /**
     * @Route("/modification_mot_passe/{id}", name="utilisateur.mot_passe")
     */
    public function modification_mot_passe(Utilisateur $utilisateur, Request $requete, UserPasswordEncoderInterface $encode,  GuardAuthenticatorHandler $garde, ConnexionAuthenticator $indentification, EntityManagerInterface $manager_interface): Response
    {

        $formulaire = $this->createForm(MotPasseFormType::class, $utilisateur, [
            "attr" => [
                "class" => "formulaire_modification_mot_passe",
            ]
        ]);

        $formulaire->handleRequest($requete);
        if ($formulaire->isSubmitted() && $formulaire->isValid()) {
            $utilisateur->setPassword(
                $encode->encodePassword(
                    $utilisateur,
                    $formulaire->get('plainPassword')->getData()
                )
            );
            $manager_interface->persist($utilisateur);
            $manager_interface->flush();


            return $garde->authenticateUserAndHandleSuccess(
                $utilisateur,
                $requete,
                $indentification,
                'main'
            );
            return $this->redirect('/profil');
        }

        return $this->render('security/modification_mot_passe.html.twig', [
            'formulaire' => $formulaire->createView(),
            'utilisateur' => $utilisateur,
        ]);
    }

    /**
     * @Route("/solde/{id}", name="utilisateur.solde")
     */
    public function solde(Utilisateur $utilisateur, Request $requete, EntityManagerInterface $manager_interface): Response
    {

        $solde_precedant = $utilisateur->getSolde();

        $formulaire = $this->createForm(SoldeFormType::class, $utilisateur, [
            "attr" => [
                "class" => "formulaire_modification_solde",
            ]
        ]);

        $formulaire->handleRequest($requete);
        if ($formulaire->isSubmitted() && $formulaire->isValid()) {
            $ajout_solde = $utilisateur->getSolde();
            $utilisateur->setSolde($solde_precedant + $ajout_solde);
            $manager_interface->persist($utilisateur);
            $manager_interface->flush();
            return $this->redirect('/profil');
        }

        return $this->render('security/modification_solde.html.twig', [
            'formulaire' => $formulaire->createView(),
            'utilisateur' => $utilisateur,
        ]);
    }

    /**
     * @Route("/supprimer/{id}", name="utilisateur.supprimer")
     */
    public function supprimer_utilisateur(Utilisateur $utilisateur, EntityManagerInterface $manager_interface): Response
    {
        $manager_interface->remove($utilisateur);
        $manager_interface->flush();
        return $this->redirect('/logout');
    }
}
