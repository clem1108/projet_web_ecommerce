<?php

namespace App\Controller;

use App\Entity\Jeu;
use App\Form\RechercheFormType;
use App\Repository\JeuRepository;
use App\Repository\UtilisateurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class AccueilController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index(JeuRepository $repertoire, Request $requete): Response
    {

        $formulaire = $this->createForm(RechercheFormType::class, [
            "attr" => [
                "class" => "formulaire_recherche",
            ]
        ]);

        $formulaire->handleRequest($requete);
        if ($formulaire->isSubmitted()) {
            $jeux = array();
            $recherche = strtolower($formulaire->get('recherche')->getData());
            $jeux_sans_recherche = $repertoire->findAll();
            foreach ($jeux_sans_recherche as $jeu) {
                $nom_jeu = strtolower($jeu->getNom());
                if ($recherche != "") {
                    if (strstr($nom_jeu, $recherche) !== false) {
                        array_push($jeux, $jeu);
                    }
                } else {
                    $jeux = $repertoire->findAll();
                }
            }
        } else {
            $jeux = $repertoire->findAll();
        }

        return $this->render('accueil/index.html.twig', [
            'jeux' => $jeux,
            'formulaire' => $formulaire->createView()
        ]);
    }
    /**
     * @Route("/achat/{id}", name="achat")
     */
    public function achat(Jeu $jeu, UtilisateurRepository $repertoire, UserInterface $UtilisateurInterface, EntityManagerInterface $manager_interface): Response
    {
        $email = $UtilisateurInterface->getUsername();

        $utilisateur = $repertoire->findOneBy([
            "email" => $email,
        ]);

        $solde = $utilisateur->getSolde();
        $quantite = $jeu->getQuantite();
        $quantite_vendue = $jeu->getQuantiteVendu();
        $prix = $jeu->getPrix();

        $jeu->setQuantiteVendu($quantite_vendue + 1);
        $jeu->setQuantite($quantite - 1);
        $utilisateur->setSolde($solde - $prix);
        $manager_interface->persist($jeu);
        $manager_interface->persist($utilisateur);
        $manager_interface->flush();
        return $this->redirect('/');
    }

    /**
     * @Route("/profil", name="profil")
     */
    public function profil(UtilisateurRepository $repertoire, UserInterface $UtilisateurInterface): Response
    {

        $email = $UtilisateurInterface->getUsername();

        $utilisateur = $repertoire->findOneBy([
            "email" => $email,
        ]);

        return $this->render('accueil/profil.html.twig', [
            'utilisateur' => $utilisateur,
        ]);
    }

    /**
     * @Route("/jeu/{id}", name="jeu.affichage")
     */
    public function jeu(Jeu $jeu, UtilisateurRepository $repertoire, UserInterface $UtilisateurInterface): Response
    {

        $email = $UtilisateurInterface->getUsername();

        $utilisateur = $repertoire->findOneBy([
            "email" => $email,
        ]);
        return $this->render('accueil/jeu.html.twig', [
            'jeu' => $jeu,
            'utilisateur' => $utilisateur
        ]);
    }
}
