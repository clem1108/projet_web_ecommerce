<?php

namespace App\Controller;

use App\Entity\Jeu;
use App\Form\JeuFormType;
use App\Repository\JeuRepository;
use App\Repository\UtilisateurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/gestion", name="gestion")
     */
    public function gestion(UtilisateurRepository $repo, JeuRepository $repo_jeu): Response
    {
        $utilisateurs = $repo->findAll();
        $nombre_utilisateurs = 0;

        foreach ($utilisateurs as $utilisateur) {
            if ($utilisateur->getRoles() == ["ROLE_USER"]) {
                $nombre_utilisateurs++;
            }
        }

        $jeux = $repo_jeu->findAll();
        $nombre_ventes = 0;
        $revenus = 0;
        foreach ($jeux as $jeu) {
            $nombre_ventes += $jeu->getQuantiteVendu();
            $revenus += $jeu->getQuantiteVendu() * $jeu->getPrix();
        }



        return $this->render('admin/index.html.twig', [
            "nombre_utilisateur" => $nombre_utilisateurs,
            "nombre_vente" => $nombre_ventes,
            "revenus" => $revenus
        ]);
    }

    /**
     * @Route("/gestion_membre", name="gestion.membre")
     */
    public function gestion_membre(UtilisateurRepository $repo): Response
    {
        $utilisateurs = $repo->findAll();

        return $this->render('admin/membre.html.twig', [
            "utilisateurs" => $utilisateurs
        ]);
    }

    /**
     * @Route("/gestion_jeu", name="gestion.jeu")
     */
    public function gestion_jeu(JeuRepository $repo): Response
    {
        $jeux = $repo->findAll();
        return $this->render('admin/jeu.html.twig', [
            "jeux" => $jeux
        ]);
    }

    /**
     * @Route("/ajout_jeu/", name="jeu.ajout")
     */
    public function ajout_jeu(Request $requete, EntityManagerInterface $manager_interface): Response
    {

        $jeu = new Jeu();
        $formulaire = $this->createForm(JeuFormType::class, $jeu);
        $formulaire->handleRequest($requete);

        if ($formulaire->isSubmitted() && $formulaire->isValid()) {
            $jeu->setQuantiteVendu(0);
            $manager_interface->persist($jeu);
            $manager_interface->flush();
            return $this->redirect('/gestion_jeu');
        }
        return $this->render('admin/ajout_jeu.html.twig', [
            'formulaire' => $formulaire->createView(),
        ]);
    }

    /**
     * @Route("/modification_jeu/{id}", name="jeu.modification")
     */
    public function modification_jeu(Jeu $jeu, Request $requete, EntityManagerInterface $manager_interface): Response
    {
        $formulaire = $this->createForm(JeuFormType::class, $jeu);
        $formulaire->handleRequest($requete);

        if ($formulaire->isSubmitted() && $formulaire->isValid()) {
            $manager_interface->persist($jeu);
            $manager_interface->flush();
            return $this->redirect('/gestion_jeu');
        }
        return $this->render('admin/modification_jeu.html.twig', [
            'formulaire' => $formulaire->createView(),
        ]);
    }

    /**
     * @Route("/supprimer_jeu/{id}", name="jeu.supprimer")
     */
    public function supprimer_jeu(Jeu $jeu, EntityManagerInterface $manager_interface): Response
    {
        $manager_interface->remove($jeu);
        $manager_interface->flush();
        return $this->redirect('/gestion_jeu');
    }
}
