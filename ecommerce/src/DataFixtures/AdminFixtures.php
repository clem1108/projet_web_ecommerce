<?php

namespace App\DataFixtures;

use App\Entity\Utilisateur;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminFixtures extends Fixture
{

    public function __construct(UserPasswordEncoderInterface $encode)
    {
        $this->encode = $encode;
    }

    public function load(ObjectManager $manager)
    {

        $admin = new Utilisateur();
        $admin->setNom("Crenn")
            ->setPrenom("Antoine")
            ->setEmail("formule1antoine@gmail.com")
            ->setDateNaissance(new DateTime("09/04/2001"))
            ->setRoles(["ROLE_ADMIN"])
            ->setSolde(0);
        $admin->setPassword($this->encode->encodePassword($admin, "antoine"));
        $admin1 = new Utilisateur();
        $admin1->setNom("Dournet")
            ->setPrenom("Clement")
            ->setEmail("clement.dournet@gmail.com")
            ->setDateNaissance(new DateTime("11/08/2001"))
            ->setRoles(["ROLE_ADMIN"])
            ->setSolde(0);
        $admin1->setPassword($this->encode->encodePassword($admin1, "clement"));
        $prof = new Utilisateur();
        $prof->setNom("Professeur")
            ->setPrenom("Professeur")
            ->setEmail("professeur@gmail.com")
            ->setDateNaissance(new DateTime("04/06/1981"))
            ->setRoles(["ROLE_ADMIN"])
            ->setSolde(40);
        $prof->setPassword($this->encode->encodePassword($prof, "professeur"));
        $manager->persist($admin);
        $manager->persist($admin1);
        $manager->persist($prof);
        $manager->flush();
    }
}
