<?php

namespace App\DataFixtures;

use App\Entity\Jeu;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class JeuFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $jeu = new Jeu();
        $jeu->setNom("Arma 3")
            ->setPrix(27.99)
            ->setDescription("Jeu de simulation militaire.")
            ->setQuantite(10)
            ->setQuantiteVendu(0);
        $jeu_1 = new Jeu();
        $jeu_1->setNom("Motorsport Manager")
            ->setPrix(34.99)
            ->setDescription("Jeu de gestion d'équipe de sport mécanique.")
            ->setQuantite(10)
            ->setQuantiteVendu(0);
        $manager->persist($jeu);
        $manager->persist($jeu_1);
        $manager->flush();
    }
}
